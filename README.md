<div align="center">
  <!-- <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

<h1 align="center">E-Shop API</h1>
</div>

It must include a README.md explaining your solution (briefly): features
your app has, libraries used, how the API works, available endpoints, etc.

## Solution:

This backend API was made in Node.js using Express.js, it's ORM is Prisma, and the DB is PostgreSQL.
I've started defining the DB entities models and then started modelling the endpoints of the API.
The project files are organized in three main layers for driving endpoints, the first one is the routes, that receives the endpoint and asings the callbacks to execute. The second layer is the controller that deals with the logic of the data processing. And finally the service layer, this one is the one which contacts whit the ORM/DB and manages the information stored.

### Built With

- [Node.js](https://reactjs.org/)
- [Express.js](https://expressjs.com/)
- [Typescript](https://www.typescriptlang.org/)
- [Prisma](https://www.prisma.io/)
- [@hapi/boom](https://hapi.dev/)
- [bcrypt](https://www.npmjs.com/package/bcrypt)
- [passport](https://www.passportjs.org/)
- [cors](https://www.npmjs.com/package/cors)

<p align="right">(<a href="#top">back to top</a>)</p>

### Middlewares:

- Passport.js: Takes charge of the authentication process in the endpoints that need to be secured.
- Error handler: If a an error occurs during executation time, this is catched by a basic logger that logs into the server console and responses an adequate answer.

### Available endpoints:

- /users: <br />
  GET - /users - Get all users
  GET - /users/{id} - Get user by ID
  POST - /users - Create a user
  PUT - /users/{id} - Update user
  DELETE - /users/{id} - Delete user

- /products: <br />
  GET - /products - Get all products
  GET - /products/{id} - Get product by ID
  POST - /products - Create a product
  PUT - /products/{id} - Update product
  DELETE - /products/{id} - Delete product

- /brand: <br />
  GET - /products - Get all products
  GET - /products/{id} - Get product by ID
  POST - /products - Create a product
  PUT - /products/{id} - Update product
  DELETE - /products/{id} - Delete product

- /auth: <br />
  POST - /auth/login - Login process, and returns a JWT

### Authentication

The app uses Passport.js library, and passport-local strategy for login, jsonwebtoken library for signing a token with the user info. Which lately will be used in secured endpoints with passport-jwt to contrast with the secret of the API.
