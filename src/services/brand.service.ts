import boom from '@hapi/boom';
import { PrismaClient, Brand } from '@prisma/client';

const orm = new PrismaClient();

const create = async (data: Brand) => {
  try {
    return await orm.brand.create({ data });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const getAll = async () => {
  try {
    return await orm.brand.findMany({});
  } catch (error) {
    throw boom.internal();
  }
};

const get = async (id: number) => {
  try {
    return await orm.brand.findUnique({ where: { id }, include: { products: true } });
  } catch (error) {
    throw boom.internal();
  }
};

const update = async (id: number, data: BrandUpdate) => {
  try {
    return await orm.brand.update({
      where: { id },
      data,
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const remove = async (id: number) => {
  try {
    return await orm.brand.delete({ where: { id } });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

export default { create, getAll, get, update, remove };

type BrandUpdate = {
  name: string;
};
