import { PrismaClient, Product } from '@prisma/client';
import boom from '@hapi/boom';

const orm = new PrismaClient();

const create = async (data: Product) => {
  try {
    return await orm.product.create({
      data,
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const getAll = async (query: QueryParams) => {
  try {
    const { limit, offset } = query;
    if (limit && offset) {
      return await orm.product.findMany({ skip: Number(offset), take: Number(limit) });
    }
    return await orm.product.findMany({ include: { brand: true } });
  } catch (error) {
    throw boom.internal();
  }
};

const get = async (id: number) => {
  try {
    return await orm.product.findUnique({
      where: { id },
      include: { brand: true },
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const update = async (id: number, data: ProductUpdate) => {
  try {
    return await orm.product.update({
      where: { id },
      data,
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const remove = async (id: number) => {
  try {
    return await orm.product.delete({
      where: { id },
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

export default { create, getAll, get, update, remove };

type ProductUpdate = {
  name?: string;
  description?: string;
  categoryId?: number;
};

type QueryParams = {
  limit?: string;
  offset?: string;
};
