import { PrismaClient, User } from '@prisma/client';
import boom from '@hapi/boom';
import bcrypt from 'bcrypt';
const orm = new PrismaClient();

const create = async (data: User) => {
  try {
    console.log({ data });
    const isEmailTaken = await orm.user.findUnique({
      where: { email: data.email },
    });
    if (isEmailTaken !== null) {
      throw boom.conflict('email already in use');
    }
    data.password = await bcrypt.hash(data.password, 10);
    return await orm.user.create({
      data,
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const getAll = async () => {
  try {
    return await orm.user.findMany({});
  } catch (error) {
    throw boom.internal();
  }
};

const getById = async (id: number) => {
  try {
    return await orm.user.findUnique({
      where: { id },
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const getByEmail = async (email: string) => {
  try {
    return await orm.user.findUnique({
      where: { email },
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const update = async (id: number, data: UserUpdate) => {
  try {
    return await orm.user.update({
      where: { id },
      data,
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

const remove = async (id: number) => {
  try {
    return await orm.user.delete({
      where: { id },
    });
  } catch (error) {
    throw boom.badRequest(error as string);
  }
};

export default { create, getAll, getById, getByEmail, update, remove };

type UserUpdate = {
  email?: string;
  username?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
};
