import express from 'express';
import cors from 'cors';
import config from './config';
import routerApi from './routes';
import { errorHandler, boomErrorHandler } from './middlewares/error.handler';

import './utils/auth';
const app = express();
app.use(express.json());
app.use(cors());

routerApi(app);

app.use('/', (req, res) => {
  res.send('<h1>Hello, this is the e-shop backend API</h1>');
});

app.use(boomErrorHandler);
app.use(errorHandler);

app.listen(config.port, () => {
  console.log(`Server is listening on port: ${config.port}`);
});
