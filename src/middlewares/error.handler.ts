import { NextFunction, Request, Response } from 'express';
import boom from '@hapi/boom';

const boomErrorHandler = (err: unknown, req: Request, res: Response, next: NextFunction) => {
  if (boom.isBoom(err)) {
    const { output } = err;
    console.error(err);
    res.status(output.statusCode).json(output.payload.error);
  }
  next(err);
};

const errorHandler = (err: unknown, req: Request, res: Response, next: NextFunction) => {
  console.error('ErrorHandler: ', err);
  if (!boom.isBoom(err)) {
    res.status(500).send('Internal Server Error');
  }
};

export { errorHandler, boomErrorHandler };
