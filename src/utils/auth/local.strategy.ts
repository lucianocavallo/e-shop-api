import { Strategy } from 'passport-local';
import bcrypt from 'bcrypt';
import userService from '../../services/user.service';
import boom from '@hapi/boom';

const LocalStrategy = new Strategy(
  {
    usernameField: 'email',
    passwordField: 'password',
  },
  async (email, password, done) => {
    try {
      let user = await userService.getByEmail(email);
      if (!user) {
        throw (boom.unauthorized(), false);
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        throw (boom.unauthorized(), false);
      }
      // @ts-ignore
      delete user.password;
      done(null, user);
    } catch (error) {
      done(error), false;
    }
  }
);

export default LocalStrategy;
