import passport from 'passport';
import LocalStrategy from './local.strategy';
import JwtStrategy from './jwt.strategy';

passport.use(LocalStrategy);
passport.use(JwtStrategy);
