import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config';

const login = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { user } = req;

    const payload = {
      sub: user?.id,
      email: user?.email,
    };
    const token = jwt.sign(payload, config.jwtSecret);
    res.status(200).json({ user, token });
  } catch (error) {
    next(error);
  }
};

export default { login };
