import { NextFunction, Request, Response } from 'express';
import userService from '../services/user.service';

const create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = await userService.create(req.body);
    res.status(201).send(user);
  } catch (error) {
    next(error);
  }
};

const getAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const users = await userService.getAll();

    res.status(201).send(users);
  } catch (error) {
    next(error);
  }
};

const getById = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const users = await userService.getById(Number(id));

    res.status(201).send(users);
  } catch (error) {
    next(error);
  }
};

const update = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const user = await userService.update(Number(id), req.body);
    res.status(200).send(user);
  } catch (error) {
    next(error);
  }
};

const remove = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const user = await userService.remove(Number(id));
    res.status(200).send(user);
  } catch (error) {
    next(error);
  }
};

export default { create, getAll, getById, update, remove };
