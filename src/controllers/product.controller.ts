import { NextFunction, Request, Response } from 'express';
import productService from '../services/product.service';

const create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const data = req.body;
    const product = await productService.create(data);
    res.status(201).send(product);
  } catch (error) {
    next(error);
  }
};

const getAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const products = await productService.getAll(req.query);
    res.status(200).send(products);
  } catch (error) {
    next(error);
  }
};

const getById = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const product = await productService.get(Number(id));
    res.status(200).send(product);
  } catch (error) {
    next(error);
  }
};

const update = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const data = req.body;

    const product = await productService.update(Number(id), data);
    res.status(200).json(product);
  } catch (error) {
    next(error);
  }
};

const remove = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const product = await productService.remove(Number(id));
    res.status(200).send(product);
  } catch (error) {
    next(error);
  }
};

export default { create, getAll, getById, update, remove };
