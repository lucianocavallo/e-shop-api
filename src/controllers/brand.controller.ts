import { NextFunction, Request, Response } from 'express';
import brandService from '../services/brand.service';

const create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const category = await brandService.create(req.body);
    res.status(201).send(category);
  } catch (error) {
    next(error);
  }
};

const getAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const categories = await brandService.getAll();
    res.status(200).send(categories);
  } catch (error) {
    next(error);
  }
};

const getById = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const category = await brandService.get(Number(id));
    res.status(200).send(category);
  } catch (error) {
    next(error);
  }
};

const update = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const data = req.body;
    console.log({ data });

    const { id } = req.params;
    const category = await brandService.update(Number(id), data);
    res.status(200).json(category);
  } catch (error) {
    next(error);
  }
};

const remove = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const category = await brandService.remove(Number(id));
    res.status(200).send(category);
  } catch (error) {
    next(error);
  }
};

export default { create, getAll, getById, update, remove };
