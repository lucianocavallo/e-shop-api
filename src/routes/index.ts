import type { Application } from 'express';
import productRouter from './products.route';
import brandRouter from './brand.route';
import userRouter from './user.route';
import authRouter from './auth.route';

function routerApi(app: Application) {
  app.use('/products', productRouter);
  app.use('/users', userRouter);
  app.use('/brands', brandRouter);
  app.use('/auth', authRouter);
}

export default routerApi;
