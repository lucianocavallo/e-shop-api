import express from 'express';
import userController from '../controllers/user.controller';
import passport from 'passport';

const router = express.Router();

router.get('/', userController.getAll);
router.get('/:id', userController.getById);
router.post('/', userController.create);
router.put('/:id', passport.authenticate('jwt', { session: false }), userController.update);
router.delete('/:id', passport.authenticate('jwt', { session: false }), userController.remove);

export default router;
