import express from 'express';
import productController from '../controllers/product.controller';
import passport from 'passport';

const router = express.Router();

router.post('/', passport.authenticate('jwt', { session: false }), productController.create);
router.get('/', productController.getAll);
router.get('/:id', productController.getById);
router.put('/:id', passport.authenticate('jwt', { session: false }), productController.update);
router.delete('/:id', passport.authenticate('jwt', { session: false }), productController.remove);

export default router;
