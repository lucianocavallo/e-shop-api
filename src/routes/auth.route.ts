import express from 'express';
import passport from 'passport';
import authController from '../controllers/auth.controller';

const router = express.Router();

router.post('/login', passport.authenticate('local', { session: false }), authController.login);

export default router;
