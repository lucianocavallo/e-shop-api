import express from 'express';
import brandController from '../controllers/brand.controller';
import passport from 'passport';

const router = express.Router();

router.post('/', passport.authenticate('jwt', { session: false }), brandController.create);
router.get('/', brandController.getAll);
router.get('/:id', brandController.getById);
router.put('/:id', passport.authenticate('jwt', { session: false }), brandController.update);
router.delete('/:id', passport.authenticate('jwt', { session: false }), brandController.remove);

export default router;
