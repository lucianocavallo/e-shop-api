namespace NodeJS {
  interface ProcessEnv {
    JWT_SECRET: string;
    PORT: number;
  }
}

namespace Express {
  interface Request {
    user: {
      id: number;
      email: string;
    };
  }
}
